Rails.application.config.middleware.use OmniAuth::Builder do
  provider :cas3,
    host: 'ee448-newcas-server-dgreenbhm.c9users.io',
    port: 8081,
    ssl: true,
    uid_key: 'uid',
    name_key: 'fullname',
    email_key: 'email',
    disable_ssl_verification: true,
    service_validate_url: '/serviceValidate'
end
