
# Log of activities to build new-casdemo

1. Base Cloud9 Rails.
2. Add another service at root for Casino (separate punch list)
3. Add `gem 'omniauth-cas3'` to `Gemfile`
4. Run `bundle` inside workspace - failed due to conflict on version of nokogirl
5. Ran `bundle update` to resolve
6. Run `rails g model User provider:string uid:string name:string email:string`
   to generate a User model which has the fields we need (it could have more)
7. Run `rake db:migrate` to apply changes to database
8. Run Create a new file called `omniauth.rb` in the `config/initializers` directory
9. See rails still runs, debug typos
10. Create a sessions controller... `rails g controller sessions`

References: 

  * <https://github.com/ralphos/omniauth-facebook-example>
  * <https://wiki.jasig.org/display/CASUM/Technical+Overview>
  * <http://tduehr.github.io/omniauth-cas3/>
  * <http://casino.rbcas.com/>
  * <https://www.tutorialspoint.com/sqlite/sqlite_using_autoincrement.htm>

Application Demo: <https://ee448-newcas-demo-dgreenbhm.c9users.io/signin>

/usr/local/rvm/gems/ruby-2.3.0/gems/omniauth-cas3-1.1.4/lib/omniauth/strategies/cas3 
comment out https on service validation