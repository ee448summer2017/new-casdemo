class SessionsController < ApplicationController
  
 def create
   auth = request.env["omniauth.auth"]
   puts "info keys:  #{auth['info'].keys.join(',')}"
   user = User.where(:provider => auth['provider'], 
                     :uid => auth['uid']).first || User.create_with_omniauth(auth)
   session[:user_id] = user.id
   # redirect to clean up URL in browser window
   # redirect_to root_url, :notice => "Signed in!"
   redirect_to "/loggedin"
 end

 def new
   redirect_to '/auth/cas3'
 end

 def destroy
   reset_session
   redirect_to root_url, notice => 'Signed out'
 end
 
 def loggedin
   @user=  User.find(session[:user_id])
 end
 
end
